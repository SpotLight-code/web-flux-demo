package com.ek.webflux2.controller;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.System.lineSeparator;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/reactive")
public class ControllerReactive {

    @Qualifier("specific")
    private final WebClient specificClient;

    @Qualifier("sync")
    private final WebClient syncClient;

    private final WebClient defaultClient;

    @SneakyThrows
    @GetMapping("/getViaSpecificClient")
    public Mono<String> getSomeData() {
        return specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class);
    }

    @SneakyThrows
    @GetMapping("/getViaSpecificClientPostProcess")
    public Mono<String> getSomeDataAndUpdateIt() {
        return specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class)
                .doOnNext(log::info) // ~ stream().peek()
                .map(e -> e + " " + UUID.randomUUID()); // ~ stream().map()
    }


    @SneakyThrows
    @GetMapping("/getReactiveSeveralRequests")
    public Flux<String> getSomeData3() {
        return Flux.range(0, 5)
                .flatMap(e -> specificClient.get()  // ~ stream().flatMap()
                        .uri("calculate")
                        .retrieve()
                        .bodyToMono(String.class));

    }

    @SneakyThrows
    @GetMapping("error/getReactiveSeveralRequests")
    public Flux<String> getSomeData3Error() {
        return Flux.range(0, 5)
                .flatMap(e -> specificClient.get()
                        .uri("calculateWithErrors")
                        .retrieve()
                        .bodyToMono(String.class))
                .onErrorContinue((e, request) -> log.warn(e.getMessage()));
    }




    private Mono<String> getRandomUUID(WebClient client) {
        return client.get()
                .uri("https://www.uuidgenerator.net/api/version1")
                .retrieve()
                .bodyToMono(String.class);
    }

    private Mono<String> getRandomUUID() {
        return getRandomUUID(defaultClient);
    }
}