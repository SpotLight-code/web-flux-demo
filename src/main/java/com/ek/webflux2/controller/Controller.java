package com.ek.webflux2.controller;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import static java.lang.System.lineSeparator;

@RestController
@Slf4j
@RequiredArgsConstructor
public class Controller {

    @Qualifier("specific")
    private final WebClient specificClient;

    @Qualifier("sync")
    private final WebClient syncClient;

    private final WebClient defaultClient;

    @SneakyThrows
    @GetMapping("/getViaSpecificClient")
    public String getSomeData() {
        return specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    @SneakyThrows
    @GetMapping("/getViaSpecificClientPostProcess")
    public String getSomeDataAndUpdateIt() {
        return specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class)
                .doOnNext(log::info) // ~ stream().peek()
                .map(e -> e + " " + UUID.randomUUID()) // ~ stream().map()
                .block();
    }

    @SneakyThrows
    @GetMapping("/error/getViaDefaultClient")
    public String getSomeDataError() {
        return defaultClient.get()
                .uri("http://localhost:8081/api/v1/data/calculate")
                .retrieve()
                .bodyToMono(String.class)
                .onErrorReturn("Some default value if error happened")
                .block();
    }

    @SneakyThrows
    @GetMapping("/error/retry/getViaDefaultClient")
    public String getSomeDataError1() {
        return defaultClient.get()
                .uri("http://localhost:8081/api/v1/data/calculate")
                .retrieve()
                .bodyToMono(String.class)
                .retry(3)
                .onErrorResume(e -> {
                    log.error(e.getMessage());
                    return defaultClient.get()
                            .uri("https://www.uuidgenerator.net/api/version1")
                            .retrieve()
                            .bodyToMono(String.class);
                })
                .block();
    }

    @SneakyThrows
    @GetMapping("/error/retry/getViaDefaultClient1")
    public String getSomeDataError2() {
        return defaultClient.get()
                .uri("http://localhost:8081/api/v1/data/calculate")
                .retrieve()
                .bodyToMono(String.class)
                .retryWhen(Retry.backoff(4, Duration.of(1, ChronoUnit.SECONDS))) // exp delay
                .onErrorResume(e -> {
                    log.error(e.getMessage());
                    return getRandomUUID();
                })
                .block();
    }

    @SneakyThrows
    @GetMapping("/getReactiveSeveralRequests")
    public List<String> getSomeData3() {
        return Flux.range(0, 5) // ~ parallelStream
                .flatMap(e -> specificClient.get()  // ~ stream().flatMap()
                        .uri("calculate")
                        .retrieve()
                        .bodyToMono(String.class))
                .collectList().block();
    }

    @SneakyThrows
    @GetMapping("error/getReactiveSeveralRequests")
    public List<String> getSomeData3Error() {
        return Flux.range(0, 5)
                .flatMap(e -> specificClient.get()
                        .uri("calculateWithErrors")
                        .retrieve()
                        .bodyToMono(String.class))
                .onErrorContinue((e, request) -> log.warn(e.getMessage()))
                .collectList().block();
    }

    @SneakyThrows
    @GetMapping("/testLazy")
    public String testLazy() {
        var lazy = Flux.range(0, 5)
                .flatMap(e -> specificClient.get()
                        .uri("calculate")
                        .retrieve()
                        .bodyToMono(String.class))
                .collectList();

        return "test lazy get";
    }


    @SneakyThrows
    @GetMapping("/testShare")
    public String shareTest() {
        var watch = new StopWatch();

        watch.start();
        var result = specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class)
                .block();

        //some stuff
        Thread.sleep(500);
        watch.stop();
        log.info("Time spent: {} ms", watch.getLastTaskTimeMillis());

        return result;
    }

    @SneakyThrows
    @GetMapping("/testShare1")
    public String shareTest1() {
        var watch = new StopWatch();

        watch.start();
        var result = specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class);


        //some stuff
        Thread.sleep(500);
        watch.stop();
        log.info("Time spent: {} ms", watch.getLastTaskTimeMillis());

        return result.block();
    }

    @SneakyThrows
    @GetMapping("/testShare2")
    public String shareTest2() {
        var watch = new StopWatch();

        watch.start();
        var result = specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class);

        result.subscribe();

        //some stuff
        Thread.sleep(500);
        watch.stop();
        log.info("Time spent: {} ms", watch.getLastTaskTimeMillis());

        return result.block();
    }

    @SneakyThrows
    @GetMapping("/testShare3")
    public String shareTest3() {
        var watch = new StopWatch();

        watch.start();
        var result = specificClient.get()
                .uri("calculate")
                .retrieve()
                .bodyToMono(String.class).share();

        Disposable subscribe = result.subscribe();

        //some stuff
        Thread.sleep(500);
        watch.stop();
        log.info("Time spent: {} ms", watch.getLastTaskTimeMillis());

        return result.block();
    }

    @SneakyThrows
    @GetMapping("/testShare4")
    public void shareTest4() {
        var watch = new StopWatch();
        watch.setKeepTaskList(false);

        watch.start();
        Mono<String> randomUUID = getRandomUUID(defaultClient);
        log.info(lineSeparator() + lineSeparator() + "Requests via default client");
        String msg = randomUUID
                .repeat(2)
                .collectList()
                .block()
                .toString();
        log.info(msg);
        watch.stop();
        log.info("time spent: " + watch.getLastTaskTimeMillis());


        watch.start();
        log.info(lineSeparator() + lineSeparator() + "Requests via default client reactive");
        msg = Flux.range(0, 3)
                .flatMap(e -> randomUUID)
                .collectList()
                .block()
                .toString();
        log.info(msg);
        watch.stop();
        log.info("time spent: " + watch.getLastTaskTimeMillis());


        watch.start();
        log.info(lineSeparator() + lineSeparator() + "Requests via default client with share");
        msg = getRandomUUID(defaultClient)
                .share()
                .repeat(2)
                .collectList()
                .block()
                .toString();
        log.info(msg);
        watch.stop();
        log.info("time spent: " + watch.getLastTaskTimeMillis());


        watch.start();
        log.info(getRandomUUID(syncClient).repeat(3).collectList().block().toString());

        watch.stop();
        log.info("time spent: " + watch.getLastTaskTimeMillis());

    }


    private Mono<String> getRandomUUID(WebClient client) {
        return client.get()
                .uri("https://www.uuidgenerator.net/api/version1")
                .retrieve()
                .bodyToMono(String.class);
    }

    private Mono<String> getRandomUUID() {
        return getRandomUUID(defaultClient);
    }
}