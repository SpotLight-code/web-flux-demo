package com.ek.webflux2.confix;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Configuration
public class WebClientConfig {

    @Bean("specific")
    public WebClient getSpecificClient(){
      return WebClient.builder()
              .baseUrl("http://localhost:8081/api/v1/data/")
              .defaultHeader("some-header", "default header")
              .filter(this::someFilter)
              .build();
    }

    @Bean("sync")
    public WebClient syncClient(){
        return WebClient.builder()
                .filter(this::callSync)
                .build();
    }

    @Bean
    public WebClient defaultClient(){
        return WebClient.create();
    }

    private Mono<ClientResponse> callSync(ClientRequest clientRequest, ExchangeFunction exchangeFunction) {
        return Mono.justOrEmpty(exchangeFunction.exchange(clientRequest).block());
    }


    private Mono<ClientResponse> someFilter(ClientRequest clientRequest, ExchangeFunction exchangeFunction) {
        if (clientRequest.method().matches("POST")) {
            clientRequest.headers().add("modify", "true");
        }
        return exchangeFunction.exchange(clientRequest);
    }
}
